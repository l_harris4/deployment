#ifndef _cGameObject_HG_
#define _cGameObject_HG_

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <string>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>
#include <vector>
#include "sMeshDrawInfo.h"


enum eTypeOfObject
{	// Ok people, here's the deal:
	SPHERE = 0,		// - it's a sphere
	PLANE = 1,		// - it's a plane
	CAPSULE = 2,    // - it's a capsule
	AABB = 3,		// 3- it's an axis-aligned box
	UNKNOWN = 99	// I don't know
};

class cGameObject
{
public:
	cGameObject();		// constructor
	~cGameObject();		// destructor
//	cGameObject(const cGameObject &obj);  // copy constructor
	glm::vec3 position;
	//glm::vec3 orientation;
	//glm::vec3 orientation2;		// HACK (will elimiate this with)
	glm::quat qOrientation;
	float scale;

	// **********************************
	// Add some physics things
	glm::vec3 vel;			// Velocity
	glm::vec3 accel;		// Acceleration
	bool bIsUpdatedInPhysics;		// 
	bool bDiscardTexture = false;
	bool bTransparentTexture = false;
	bool isSkybox = false;
	eTypeOfObject typeOfObject;		// (really an int)
	float radius;
	bool bIsVisible = true;
	std::vector<sMeshDrawInfo> vecMeshes;
	glm::quat getFinalMeshQOrientation(unsigned int meshID);
	glm::quat getFinalMeshQOrientation(glm::quat &meshQOrientation);
	// Our "child" objects
	std::vector< cGameObject* > vec_pChildObjects;
	void DeleteChildren(void);
	// Returns NULL if not found
	cGameObject* FindChildByFriendlyName(std::string name);
	cGameObject* FindChildByID(unsigned int ID);
	// **********************************

	bool bIsWireFrame;

	glm::vec4 diffuseColour;	// 

	std::string meshName;		// mesh I'd like to draw
	
	// The texture of this object
	static const unsigned int NUMTEXTURES = 10;
	std::string textureNames[NUMTEXTURES];
	float textureBlend[NUMTEXTURES];			// 0 - 1

	std::string friendlyName;
	inline unsigned int getUniqueID(void) { return this->m_UniqueID; }
	void overwriteQOrientationFormEuler(glm::vec3 eulerAxisOrientation);
	void adjustQOrientationFormDeltaEuler(glm::vec3 eulerAxisOrientChange);
private:
	unsigned int m_UniqueID;
	// Used when creating objects
	static unsigned int m_nextUniqueID;

};

#endif
