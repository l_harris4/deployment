#include "cNode.h"
#include <math.h>
#include <iostream>

cNode::cNode()
{
	isValid = false;
	hValue = 0;
	gValue = 0;
	fValue = 0;
}

cNode::cNode(std::string id)
{
	this->Id = id;
}

cNode::~cNode()
{
}

void cNode::AddConnection(cNode& nodeToAdd)
{
	this->connectedNodes.push_back(&nodeToAdd);
}

float cNode::DistanceToNode(cNode & nodeComparing)
{
	return sqrt(pow((xLoc - nodeComparing.xLoc), 2) + pow((zLoc - nodeComparing.zLoc), 2));
}

void cNode::findPathTo(std::string targetID, cNode* currentNode, std::string IDsInPath, int currentJumpNum)
{
	//if this is the first run through
	if (currentJumpNum == 0)
	{
		lowestJumpNum = -1;
		shortestPath = "";
		currentNode = this;
	}

	//if the destination node has been found
	if (currentNode->Id == targetID)
	{
		//either the first path to get here, or the current path is shorter
		if (lowestJumpNum == -1 || (currentJumpNum < lowestJumpNum && lowestJumpNum != -1))
		{
			this->lowestJumpNum = currentJumpNum;
			IDsInPath = IDsInPath + " " + currentNode->Id;
			this->shortestPath = IDsInPath;
		}
	}
	else
	{
		currentJumpNum++;
		//if a shorter path has already been found, dont bother continuing
		if (!(currentJumpNum > lowestJumpNum && lowestJumpNum != -1))
		{
			IDsInPath = IDsInPath + " " + currentNode->Id;

			std::cout << "trying path" << IDsInPath << std::endl;
			//call this function passing all connected nodes
			for (int i = 0; i < currentNode->connectedNodes.size(); i++)
			{
				//if the node has not already been traversed by this path
				if (IDsInPath.find(currentNode->connectedNodes[i]->Id) == std::string::npos)
				{
					findPathTo(targetID, currentNode->connectedNodes[i], IDsInPath, currentJumpNum);
				}
			}
		}
	}


}
