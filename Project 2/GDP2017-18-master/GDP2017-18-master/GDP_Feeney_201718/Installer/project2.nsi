!include nsDialogs.nsh
!include LogicLib.nsh
!include ZipDLL.nsh

OutFile "myinstaller.exe"
Name "Video game 01"

InstallDir "c:\cnd\HarrisLuke\videogame"

XPStyle on

LicenseText "License"
LicenseData ".\license.txt"

Var Dialog
Var Checkbox
Var Checkbox_State

Page custom welcomePage
Page License
Page custom extraAssets extraAssetsLeave
Page Instfiles

#NOT MY METHOD
#The following method was taken from the internet from http://stefan.bertels.org/
!macro GetCleanDir INPUTDIR
  !define Index_GetCleanDir 'GetCleanDir_Line${__LINE__}'
  Push $R0
  Push $R1
  StrCpy $R0 "${INPUTDIR}"
  StrCmp $R0 "" ${Index_GetCleanDir}-finish
  StrCpy $R1 "$R0" "" -1
  StrCmp "$R1" "\" ${Index_GetCleanDir}-finish
  StrCpy $R0 "$R0\"
${Index_GetCleanDir}-finish:
  Pop $R1
  Exch $R0
  !undef Index_GetCleanDir
!macroend

#NOT MY METHOD
#The following method was taken from the internet from http://stefan.bertels.org/
!macro RemoveFilesAndSubDirs DIRECTORY
  !define Index_RemoveFilesAndSubDirs 'RemoveFilesAndSubDirs_${__LINE__}'
 
  Push $R0
  Push $R1
  Push $R2
 
  !insertmacro GetCleanDir "${DIRECTORY}"
  Pop $R2
  FindFirst $R0 $R1 "$R2*.*"
${Index_RemoveFilesAndSubDirs}-loop:
  StrCmp $R1 "" ${Index_RemoveFilesAndSubDirs}-done
  StrCmp $R1 "." ${Index_RemoveFilesAndSubDirs}-next
  StrCmp $R1 ".." ${Index_RemoveFilesAndSubDirs}-next
  IfFileExists "$R2$R1\*.*" ${Index_RemoveFilesAndSubDirs}-directory
  ; file
  Delete "$R2$R1"
  goto ${Index_RemoveFilesAndSubDirs}-next
${Index_RemoveFilesAndSubDirs}-directory:
  ; directory
  RMDir /r "$R2$R1"
${Index_RemoveFilesAndSubDirs}-next:
  FindNext $R0 $R1
  Goto ${Index_RemoveFilesAndSubDirs}-loop
${Index_RemoveFilesAndSubDirs}-done:
  FindClose $R0
 
  Pop $R2
  Pop $R1
  Pop $R0
  !undef Index_RemoveFilesAndSubDirs
!macroend

Function welcomePage
nsdialogs::Create 1018
Pop $Dialog
${If} $Dialog == error
	Abort
${EndIf}

#create label
${NSD_CreateLabel} 0 0 100% 12u "Welcome to my videogame installer"

#create another label with my name
${NSD_CreateLabel} 0 13u 100% 12u "Luke Harris"

#show dialog
nsDialogs::Show

FunctionEnd

Function .onInit
#set the registry view for setting the registry later
SetRegView 64
FunctionEnd

Function extraAssets
nsdialogs::Create 1018
Pop $Dialog
${If} $Dialog == error
	Abort
${EndIf}

#create label
${NSD_CreateLabel} 0 0 100% 12u "Include extra assets?"

${NSD_CreateCheckbox} 0 30u 100% 10u "Include extra assets?"
Pop $Checkbox

#save checkbox value
${If} $Checkbox_State == ${BST_CHECKED}
	${NSD_Check} $Checkbox
${EndIf}

#show dialog
nsDialogs::Show

FunctionEnd

Function extraAssetsLeave
#read selected values
${NSD_GetState} $Checkbox $Checkbox_State

FunctionEnd


Section

#write the path to the exe for the registry
WriteRegStr HKLM "Software\INFO6025\HarrisLuke" "videogame" 'C:\CND\HarrisLuke\videogame\project02\GDP_Feeney_201718.exe'
#get the zip file
SetOutPath $INSTDIR
File /nonfatal /a /r "c:\cnd\HarrisLuke\project02.zip"

#create uninstaller
WriteUninstaller $INSTDIR\uninstall.exe
#unzip the file
!insertmacro ZIPDLL_EXTRACT "c:\cnd\HarrisLuke\videogame\project02.zip" "c:\cnd\HarrisLuke\videogame\project02\" "<ALL>"
#attempt to delete the zip
!insertmacro RemoveFilesAndSubDirs "C:\CND\HarrisLuke\videogame\project02.zip"

${If} $Checkbox_State != ${BST_CHECKED}
	#delete the extra assets if they didnt click the box
	!insertmacro RemoveFilesAndSubDirs "C:\CND\HarrisLuke\videogame\project02\exe\extra_assets"
${EndIf}

#create the start menu shortcut
SetOutPath "$INSTDIR\project02\exe"
CreateShortCut "$SMPROGRAMS\mygame.lnk" "C:\CND\HarrisLuke\videogame\project02\exe\GDP_Feeney_201718.exe"
SetOutPath $INSTDIR

SectionEnd


Section "Uninstall"
#set the registry view 
SetRegView 64
#delete the registry key
DeleteRegKey HKLM "Software\INFO6025"
#delete the start menu link
Delete $SMPROGRAMS\mygame.lnk
#delete everything in c:\cnd\HarrisLuke
#!insertmacro RemoveFilesAndSubDirs "C:\CND\HarrisLuke"
RMDir /r "C:\CND\HarrisLuke"

SectionEnd