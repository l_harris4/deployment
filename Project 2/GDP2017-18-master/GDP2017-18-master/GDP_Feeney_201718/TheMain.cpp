// Include glad and GLFW in correct order
#include "globalOpenGL_GLFW.h"


#include <iostream>			// C++ cin, cout, etc.
//#include "linmath.h"
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp> // glm::value_ptr
#include "nlohmann\json.hpp"

#include <stdlib.h>
#include <stdio.h>
// Add the file stuff library (file stream>
#include <fstream>
#include <sstream>		// "String stream"
#include <string>
#include <time.h>

#include <vector>		//  smart array, "array" in most languages
#include "Utilities.h"
#include "ModelUtilities.h"
#include "cMesh.h"
#include "cShaderManager.h" 
#include "cGameObject.h"
#include "cVAOMeshManager.h"
#include "cModelAssetLoader.h"
#include <algorithm>
#include "cCamera.h"
#include "cNode.h"
#include <list>
#include <algorithm>
#include <gtest\gtest.h>

#include <ft2build.h>
//#include <freetype/freetype.h>
#include FT_FREETYPE_H

//#include "pugixml.hpp"
#include "pugixml.hpp"


#include "Physics.h"	// Physics collision detection functions

#include "cLightManager.h"

// Include all the things that are accessed in other files
#include "globalGameStuff.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>
#include "mydll.h"

//void DrawParticle(cParticle* pThePart);
void setCubeSamplerAndBlenderByIndex(GLint samplerIndex, float blendRatio, GLint textureUnitID);

extern void updateCamera();



// Used by the light drawing thingy
// Will draw a wireframe sphere at this location with this colour
//void DrawDebugSphere(glm::vec3 location, glm::vec4 colour, float scale);
//cGameObject* g_pTheDebugSphere;

FT_Library mft;

FT_Face mface;

// Remember to #include <vector>...
std::vector< cGameObject* >  g_vecGameObjects;
std::vector<cNode> allNodes;
glm::vec2 nodeStart;
glm::vec2 nodeEnd;
cNode* startingNode;
cNode* endingNode;
cNode* currentNode;
const float ACCEPTABLEDISTANCE = 2.5f;
bool mainObjectMoving = false;
bool findingPath = true;
glm::vec3 objectDestination;

std::vector<std::string> loadedText;
int selectedLanguage = 6;

bool rightPathFromStart = true;
bool pathExists = true;
int pathCounter = 0;
std::vector<cNode*> rightPathNodes;


std::list<cNode*> openNodes;
std::list<cNode*> closedNodes;


cVAOMeshManager* g_pVAOManager = 0;		// or NULL, or nullptr

//cShaderManager	g_ShaderManager;			// Stack (no new)
cShaderManager*		g_pShaderManager = 0;		// Heap, new (and delete)
cLightManager*		g_pLightManager = 0;
CTextureManager*		g_pTextureManager = 0;

cDebugRenderer*			g_pDebugRenderer = 0;
cCamera* g_pTheCamera = NULL;


// Other uniforms:
GLint uniLoc_materialDiffuse = -1;
GLint uniLoc_materialAmbient = -1;
GLint uniLoc_ambientToDiffuseRatio = -1; 	// Maybe	// 0.2 or 0.3
GLint uniLoc_materialSpecular = -1;  // rgb = colour of HIGHLIGHT only
							// w = shininess of the 
GLint uniLoc_bIsDebugWireFrameObject = -1;
GLint uniLoc_bUsingLighting = -1;
GLint uniLoc_bUsingTextures = -1;
GLint uniLoc_bDiscardTexture = -1;

GLint uniLoc_eyePosition = -1;	// Camera position
GLint uniLoc_mModel = -1;
GLint uniLoc_mView = -1;
GLint uniLoc_mProjection = -1;

GLint texSampCube00_LocID = -1;
GLint texSampCube01_LocID = -1;
GLint texSampCube02_LocID = -1;
GLint texSampCube03_LocID = -1;

GLint texCubeBlend00_LocID = -1;
GLint texCubeBlend01_LocID = -1;
GLint texCubeBlend02_LocID = -1;
GLint texCubeBlend03_LocID = -1;

int g_selectedGameObjectIndex = 0;
int g_selectedLightIndex = 0;
bool g_movingGameObject = false;
bool g_lightsOn = false;
bool g_texturesOn = false;
bool g_movingLights = false;
bool g_boundingBoxes = false;
const float MOVESPEED = 0.1f;
const float ROTATIONSPEED = -2;
const float CAMERASPEED = 0.2f;


const char* vertexShaderText =
"#version 410\n"
"attribute vec4 coord;"
"varying vec2 texpos;"
"void main () {"
"	gl_Position = vec4(coord.xy, 0, 1);"
"	texpos = coord.zw;"
"}";

const char* fragmentShaderText =
"#version 410\n"
"varying vec2 texpos;"
"uniform bool igcolour;"
"uniform sampler2D tex;"
"uniform vec4 color;"
"void main () {"
" if(igcolour){"
"	gl_FragColor = vec4(texture2D(tex, texpos).xyz, 1);"
"}else{"
"	gl_FragColor = vec4(1, 1, 1, texture2D(tex, texpos).r) * color;"
"	}"
"}";

GLint attribute_coord;
GLint uniform_igcolour;
GLint uniform_tex;
GLint uniform_color;
GLuint mdp_vbo;
GLuint vertexShader;
GLuint fragmentShader;
GLuint textProgram;

//vertex array object
GLuint mvao;
GLuint mwidth = 0;
GLFWwindow* window;
GLuint mheight = 0;
//vertex buffer object
//cGameObject* cameraTargetObject = new cGameObject();


static void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

struct DistanceFunc
{
	DistanceFunc(const glm::vec3& _p) : p(_p) {}

	bool operator()(const cGameObject* lhs, const cGameObject* rhs) const
	{
		return glm::distance(p, lhs->position) > glm::distance(p, rhs->position);
	}

private:
	glm::vec3 p;
};

void loadText(std::string input_xml_file)
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(input_xml_file.c_str());

	pugi::xml_node_iterator it = doc.children().begin();

	for (it; it != doc.children().end(); it++)
	{
		//we will find language children here
		pugi::xml_node_iterator it2 = it->children().begin();
		for (it2; it2 != it->children().end(); it2++)
		{
			loadedText.push_back(it2->child_value());
		}

	}
}

void loadTextJSON(std::string inputJSONFile)
{
	// Open file 
	std::ifstream inFile(inputJSONFile);
	
	// Create json Object
	nlohmann::json jsonMaster;

	// Inject file into json Object
	inFile >> jsonMaster;

	for (int i = 0; i < jsonMaster["Languages"]["English"].size(); ++i)
	{
		std::string test = jsonMaster["Languages"]["English"][i].get<std::string>();
		loadedText.push_back(test);
	}

	for (int i = 0; i < jsonMaster["Languages"]["Swahili"].size(); ++i)
	{
		std::string test = jsonMaster["Languages"]["Swahili"][i].get<std::string>();
		loadedText.push_back(test);
	}

	for (int i = 0; i < jsonMaster["Languages"]["Xhosa"].size(); ++i)
	{
		std::string test = jsonMaster["Languages"]["Xhosa"][i].get<std::string>();
		loadedText.push_back(test);
	}

	for (int i = 0; i < jsonMaster["Languages"]["Zulu"].size(); ++i)
	{
		std::string test = jsonMaster["Languages"]["Zulu"][i].get<std::string>();
		loadedText.push_back(test);
	}

	for (int i = 0; i < jsonMaster["Languages"]["Somali"].size(); ++i)
	{
		std::string test = jsonMaster["Languages"]["Somali"][i].get<std::string>();
		loadedText.push_back(test);
	}
}

GLboolean initfreetype() {

	if (FT_Init_FreeType(&mft))
	{
		fprintf(stderr, "unable to init free type\n");
		return GL_FALSE;
	}

	
	//if (FT_New_Face(mft, ".\\assets\\fonts\\FreeSans.ttf", 0, &mface))
	if (FT_New_Face(mft, "assets/fonts/Digitalt.ttf", 0, &mface))
	{
		fprintf(stderr, "unable to open font\n");
		return GL_FALSE;
	}

	//set font size
	FT_Set_Pixel_Sizes(mface, 0, 48);


	if (FT_Load_Char(mface, 'X', FT_LOAD_RENDER))
	{
		fprintf(stderr, "unable to load character\n");
		return GL_FALSE;
	}


	return GL_TRUE;
}

GLboolean init_gl() {

	//create shaders
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexShaderText, NULL);
	glCompileShader(vertexShader);

	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderText, NULL);
	glCompileShader(fragmentShader);

	textProgram = glCreateProgram();
	glAttachShader(textProgram, vertexShader);
	glAttachShader(textProgram, fragmentShader);

	glLinkProgram(textProgram);

	//get vertex attribute/s id/s
	attribute_coord = glGetAttribLocation(textProgram, "coord");
	uniform_igcolour = glGetUniformLocation(textProgram, "igcolour");
	uniform_tex = glGetUniformLocation(textProgram, "tex");
	uniform_color = glGetUniformLocation(textProgram, "color");

	if (attribute_coord == -1 || uniform_tex == -1 || uniform_color == -1 || uniform_igcolour == -1) {
		fprintf(stderr, "unable to get attribute or uniform from shader\n");
		return GL_FALSE;
	}

	//generate and bind vbo 
	glGenBuffers(1, &mdp_vbo);

	//generate and bind vao
	glGenVertexArrays(1, &mvao);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDepthMask(GL_TRUE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	return GL_TRUE;
}

static void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	mwidth = width;
	mheight = height;
	glViewport(0, 0, width, height);
}

GLboolean init_glfw() {

	//set error call back method
	glfwSetErrorCallback(error_callback);
	//init glfw
	if (!glfwInit()) {
		fprintf(stderr, "Unable to start glfw!");
		return GL_FALSE;

	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	//init window
	mwidth = 1024;
	mheight = 600;

	window = glfwCreateWindow(mwidth, mheight, "Media fundamentals...", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		fprintf(stderr, "Unable to create window!");
		return GL_FALSE;
	}
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetKeyCallback(window, key_callback);

	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		fprintf(stderr, "Unable to init glad!");
		glfwTerminate();
		return GL_FALSE;

	}

	return GL_TRUE;
}


#if (_DEBUG)
#ifdef WIN32
//white box tests
//Test that each node has at least one node attached
TEST(AttachedNodes, Nodes) {
	
	bool attached = true;
	for (int i = 0; i < allNodes.size(); ++i)
	{
		if (allNodes[i].connectedNodes.size() == 0)
		{
			attached = false;
		}
	}
	EXPECT_TRUE(attached);
}

//Test that the glm distance function is working pretty well
TEST(Distance, Nodes) {

	glm::vec2 first = glm::vec2(0, 0);
	glm::vec2 second = glm::vec2(2, 2);
	float dist = glm::distance(first,second);
	EXPECT_LT(1, dist);
}

//Test that path exists //because if no path exists then the pathfinding will not work
TEST(Path, Nodes) {
	ASSERT_TRUE(pathExists);
}

//test that the text file that loads textures exists
TEST(FileTextures, Nodes) {
	std::string filename = "MeshConfig.txt";
	std::string line;
	std::ifstream inFile(filename.c_str());

	ASSERT_TRUE(inFile.good());
}

//test that the text file that loads stuff exists
TEST(FileObjects, Nodes) {
	std::string filename = "ObjectsConfig.txt";
	std::string line;
	std::ifstream inFile(filename.c_str());

	ASSERT_TRUE(inFile.good());
}

//test that if it is the right path from start, and the path exists then the container rightPathNodes should be populated
TEST(PathExists, Nodes) {

	if (rightPathFromStart && pathExists)
		EXPECT_TRUE(rightPathNodes.size() > 0);
}
//test that starting node should not equal ending node, because then there would be no point to pathfinding
TEST(NotEmpty, Nodes) {
	EXPECT_NE(startingNode, endingNode);
}
//test that at least one node exists
TEST(NotEmpty2, Nodes) {
	EXPECT_TRUE(allNodes.size() > 0);
}

//test that a node exists with the same location as the start
TEST(StartExists, Nodes) {
	bool nodeStartExists = false;
	for (int i = 0; i < allNodes.size(); ++i)
	{
		if (allNodes[i].xLoc == nodeStart.x && allNodes[i].zLoc == nodeStart.y)
		{
			nodeStartExists = true;
		}
	}
	ASSERT_TRUE(nodeStartExists);
}
//test that a node exists with the same location as the end
TEST(EndExists, Nodes) {
	bool nodeEndExists = false;
	for (int i = 0; i < allNodes.size(); ++i)
	{
		if (allNodes[i].xLoc == nodeEnd.x && allNodes[i].zLoc == nodeEnd.y)
		{
			nodeEndExists = true;
		}
	}
	ASSERT_TRUE(nodeEndExists);
}


//light manager genUniName
//AddConnection for node


//black box tests
//Test that the adding function for integers is working
TEST(addMyMath, Math) {
	int result = MyMath::Functions::add(1, 2);
	ASSERT_EQ(result,3);
}

//Test that the subtract function for integers is working
TEST(subtractMyMath, Math) {
	int result = MyMath::Functions::subtract(1, 2);
	ASSERT_EQ(result, -1);
}

//Test that the multiply function for integers is working
TEST(multiplyMyMath, Math) {
	int result = MyMath::Functions::multiply(1, 2);
	ASSERT_EQ(result, 2);
}

//Test that the divide function for integers is working
TEST(divideIntMath, Math) {
	int result = MyMath::Functions::divide(1, 2);
	ASSERT_EQ(result, 0);
}

//Test that the divide function for floats is working
TEST(divideFloatMath, Math) {
	float result = MyMath::Functions::divide(1.0f, 2.0f);
	ASSERT_EQ(result, 0.5f);
}

//Test that the equal function for integers is working
TEST(equal, Math) {
	bool result = MyMath::Functions::equal(1, 1);
	EXPECT_TRUE(result);
}

//Test that the lessThan function for integers is working
TEST(less, Math) {
	bool result = MyMath::Functions::lessThan(1, 2);
	EXPECT_TRUE(result);
}

//Test that the lessThan function returns appropriate value when given equal results
TEST(less2, Math) {
	bool result = MyMath::Functions::lessThan(1, 1);
	EXPECT_FALSE(result);
}

//Test that the greaterThan function is working given normal case
TEST(greater, Math) {
	bool result = MyMath::Functions::greaterThan(2, 1);
	EXPECT_TRUE(result);
}

//Test that the greaterThan function return appropriate value when given equal results
TEST(greater2, Math) {
	bool result = MyMath::Functions::greaterThan(1, 1);
	EXPECT_FALSE(result);
}
#endif
#endif


int main(int argc, char **argv)
{
	//init_glfw();
	initfreetype();
	
	//loadText("assets/xml/intro.xml");
	loadTextJSON("assets/json/intro.json");
	srand(time(NULL));
	GLFWwindow* window;
	//    GLuint vertex_buffer, vertex_shader, fragment_shader, program;
	GLint mvp_location;	// , vpos_location, vcol_location;
	glfwSetErrorCallback(error_callback);

	if (!glfwInit())
	{
		// exit(EXIT_FAILURE);
		std::cout << "ERROR: Couldn't init GLFW, so we're pretty much stuck; do you have OpenGL??" << std::endl;
		return -1;
	}

	

	int height = 700;	/* default */
	int width = 1200;	// default
	std::string title = "DEPLOYMENT PROJECT 1";


	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	// C++ string
	// C no strings. Sorry. char    char name[7] = "Michael\0";
	window = glfwCreateWindow(width, height,
		title.c_str(),
		NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwSetKeyCallback(window, key_callback);
	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	glfwSwapInterval(1);

	std::cout << glGetString(GL_VENDOR) << " "
		<< glGetString(GL_RENDERER) << ", "
		<< glGetString(GL_VERSION) << std::endl;
	std::cout << "Shader language version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

	// General error string, used for a number of items during start up
	std::string error;

	::g_pShaderManager = new cShaderManager();

	cShaderManager::cShader vertShader;
	cShaderManager::cShader fragShader;

	vertShader.fileName = "simpleVert.glsl";
	fragShader.fileName = "simpleFrag.glsl";

	::g_pShaderManager->setBasePath("assets//shaders//");

	// Shader objects are passed by reference so that
	//	we can look at the results if we wanted to. 
	if (!::g_pShaderManager->createProgramFromFile(
		"mySexyShader", vertShader, fragShader))
	{
		std::cout << "Oh no! All is lost!!! Blame Loki!!!" << std::endl;
		std::cout << ::g_pShaderManager->getLastError() << std::endl;
		// Should we exit?? 
		return -1;
		//		exit(
	}
	std::cout << "The shaders comipled and linked OK" << std::endl;
	init_gl();

	::g_pDebugRenderer = new cDebugRenderer();
	if (!::g_pDebugRenderer->initialize(error))
	{
		std::cout << "Warning: couldn't init the debug renderer." << std::endl;
	}

	// Load models
	::g_pModelAssetLoader = new cModelAssetLoader();
	::g_pModelAssetLoader->setBasePath("assets/models/");

	std::cout << "1" << std::endl;
	::g_pVAOManager = new cVAOMeshManager();

	GLint sexyShaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	if (!Load3DModelsIntoMeshManager(sexyShaderID, ::g_pVAOManager, ::g_pModelAssetLoader, error))
	{
		std::cout << "Not all models were loaded..." << std::endl;
		std::cout << error << std::endl;
	}

	std::cout << "2" << std::endl;
	//LoadModelsIntoScene();
	::g_pLightManager = new cLightManager();
	::g_pTheCamera = new cCamera();
	::g_pTheCamera->eye = glm::vec3(-10.0f, 20.0f, 10.0f);
	::g_pTheCamera->cameraMode = ::cCamera::eMode::FOLLOW_CAMERA;
	::g_pTheCamera->target = glm::vec3(0, 0, 0);
	::g_pTheCamera->theObject = new cGameObject();


	GLint currentProgID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");
	::g_pTextureManager = new CTextureManager();


	::g_pTextureManager->setBasePath("assets/textures");


	LoadModelsLightsFromFile();

	::g_pTextureManager->setBasePath("assets/textures/skybox");
	if (!::g_pTextureManager->CreateCubeTextureFromBMPFiles(
		"space",
		"SpaceBox_right1_posX.bmp",
		"SpaceBox_left2_negX.bmp",
		"SpaceBox_top3_posY.bmp",
		"SpaceBox_bottom4_negY.bmp",
		"SpaceBox_front5_posZ.bmp",
		"SpaceBox_back6_negZ.bmp", true, true))
	{
		std::cout << "Didn't load skybox" << std::endl;
	}


	std::cout << "3" << std::endl;
	::g_pLightManager->LoadShaderUniformLocations(currentProgID);


	//connect adjacent nodes
	//go through all nodes and if another node is close enough, identify it as a connected node
	int numNodes = allNodes.size();
	for (int i = 0; i < numNodes; i++)
	{
		glm::vec2 tempPos;
		tempPos.x = allNodes[i].xLoc;
		tempPos.y = allNodes[i].zLoc;

		if (tempPos == nodeStart)
			startingNode = &allNodes[i];

		if (tempPos == nodeEnd)
			endingNode = &allNodes[i];

		//also calculate the h value for the node
		allNodes[i].hValue += abs(nodeEnd.x - tempPos.x) / 2;
		allNodes[i].hValue += abs(nodeEnd.y - tempPos.y) / 2;

		for (int j = 0; j < numNodes; j++)
		{
			glm::vec2 firstPos;
			firstPos.x = allNodes[j].xLoc;
			firstPos.y = allNodes[j].zLoc;
			//if the other node is pretty close, add it to connected nodes
			if (i != j && allNodes[j].isValid)
			{
				glm::vec2 secondPos;
				secondPos.x = allNodes[i].xLoc;
				secondPos.y = allNodes[i].zLoc;
				if (glm::distance(firstPos, secondPos) < ACCEPTABLEDISTANCE)
				{
					allNodes[i].connectedNodes.push_back(&allNodes[j]);
				}
			}
		}
	}

	std::cout << "4" << std::endl;
	/*if (endingNode == 0)
		endingNode = startingNode;*/

	
	//Now place the flag objects for current positon, starting position, and ending position
	g_vecGameObjects[1]->position.x = startingNode->xLoc;
	g_vecGameObjects[1]->position.z = startingNode->zLoc;
	g_vecGameObjects[1]->position.y = 2;

	g_vecGameObjects[2]->position.x = nodeEnd.x;
	g_vecGameObjects[2]->position.z = nodeEnd.y;
	g_vecGameObjects[2]->position.y = 2;

	g_vecGameObjects[3]->position.x = startingNode->xLoc;
	g_vecGameObjects[3]->position.z = startingNode->zLoc;
	g_vecGameObjects[3]->position.y = 2;

	objectDestination.x = nodeEnd.x;
	objectDestination.z = nodeEnd.y;
	objectDestination.y = 2;

	currentNode = startingNode;

	std::cout << "5" << std::endl;
	//if we only want to see the calculate path, then have to calculate it before rendering
	if (rightPathFromStart)
	{
		while (currentNode != endingNode)
		{
			//this is where the pathfinding logic will take place
			closedNodes.push_back(currentNode);
			//take self out of open nodes
			openNodes.remove(currentNode);


			for (int i = 0; i < currentNode->connectedNodes.size(); ++i)
			{
				//this is so we dont reprocess a node
				if (currentNode->connectedNodes[i]->fValue == 0)
				{
					//process the node
					currentNode->connectedNodes[i]->gValue = currentNode->gValue + 1;
					currentNode->connectedNodes[i]->fValue = currentNode->connectedNodes[i]->gValue + currentNode->connectedNodes[i]->hValue;
					openNodes.push_back(currentNode->connectedNodes[i]);
				}
			}

			if (openNodes.empty())
			{
				pathExists = false;
				break;
			}
			cNode* nextNode = openNodes.back();
			int lowestF = nextNode->fValue;
			//now go through the open nodes and find the one with least f value 
			for (std::list<cNode*>::iterator iterator = openNodes.begin(), end = openNodes.end(); iterator != end; ++iterator) {
				if ((*iterator)->fValue < lowestF)
				{
					lowestF = (*iterator)->fValue;
					nextNode = (*iterator);
				}
				if ((*iterator) == endingNode)
				{
					nextNode = endingNode;
					break;
				}
			}
			//set that to current node
			currentNode = nextNode;

		}

		rightPathNodes.push_back(currentNode);
		//first run through current node should be the end node
		while (currentNode != startingNode)
		{
			std::vector<cNode*> backtrackNodes;
			//to pick the next node we have to pick a node that is adjacent and in the list of closed nodes
			for (int i = 0; i < currentNode->connectedNodes.size(); ++i)
			{
				if (currentNode->connectedNodes[i] == startingNode)
				{
					backtrackNodes.clear();
					backtrackNodes.push_back(currentNode->connectedNodes[i]);
					break;
				}
				if (std::find(closedNodes.begin(), closedNodes.end(), currentNode->connectedNodes[i]) != closedNodes.end())
				{
					backtrackNodes.push_back(currentNode->connectedNodes[i]);
				}
			}
			//if there are multiple then have to select the one with the lowest g value
			if (backtrackNodes.size() > 1)
			{
				cNode* selected = backtrackNodes[0];
				for (int i = 1; i < backtrackNodes.size(); ++i)
				{
					if (backtrackNodes[i]->gValue < selected->gValue)
						selected = backtrackNodes[i];
				}
				currentNode = selected;
			}
			else
			{
				currentNode = backtrackNodes[0];
			}

			rightPathNodes.push_back(currentNode);
		}
		currentNode = rightPathNodes[rightPathNodes.size() - 2];
		mainObjectMoving = true;
		pathCounter = rightPathNodes.size() - 2;
	}



	

	glEnable(GL_DEPTH);

	// Gets the "current" time "tick" or "step"
	double lastTimeStep = glfwGetTime();


#if (_DEBUG)
#ifdef WIN32
	::testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
#endif
#endif
	GLfloat red[4] = { 1, 0, 0, 1 };

	// Main game or application loop
	while (!glfwWindowShouldClose(window))
	{
		//sortObjectsBasedOnCamera();
		// Essentially the "frame time"
		// Now many seconds that have elapsed since we last checked
		double curTime = glfwGetTime();
		double deltaTime = curTime - lastTimeStep;

		::g_pTheCamera->updateTick(deltaTime);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		RenderScene(::g_vecGameObjects, window, deltaTime);

		// "Presents" what we've drawn
		// Done once per scene 
		glfwSwapBuffers(window);
		glfwPollEvents();

		if (rightPathFromStart && pathExists)
		{
			objectDestination.x = currentNode->xLoc;
			objectDestination.z = currentNode->zLoc;
			objectDestination.y = 2;
			if (mainObjectMoving)
			{
				::g_vecGameObjects[1]->position += glm::normalize(objectDestination - ::g_vecGameObjects[1]->position) * MOVESPEED;
				if (glm::distance(::g_vecGameObjects[1]->position, objectDestination) <= MOVESPEED)
				{
					mainObjectMoving = false;
					::g_vecGameObjects[1]->position = objectDestination;
				}
				::g_pTheCamera->theObject = ::g_vecGameObjects[1];
				::g_pTheCamera->target = ::g_vecGameObjects[1]->position;
			}
			else if (currentNode != endingNode)
			{
				pathCounter--;
				currentNode = rightPathNodes[pathCounter];
				mainObjectMoving = true;
			}

		}
		else if (pathExists)
		{
			//update the main objects location if it not there
			if (mainObjectMoving)
			{

				::g_vecGameObjects[1]->position += glm::normalize(objectDestination - ::g_vecGameObjects[1]->position) * MOVESPEED;
				if (glm::distance(::g_vecGameObjects[1]->position, objectDestination) <= MOVESPEED)
				{
					mainObjectMoving = false;
					::g_vecGameObjects[1]->position = objectDestination;
				}
				::g_pTheCamera->theObject = ::g_vecGameObjects[1];
				::g_pTheCamera->target = ::g_vecGameObjects[1]->position;
				
			}
			else if (findingPath)//otherwise find the next node to travel to
			{
				if (currentNode != endingNode)
				{
					//this is where the pathfinding logic will take place
					closedNodes.push_back(currentNode);
					//take self out of open nodes
					openNodes.remove(currentNode);


					for (int i = 0; i < currentNode->connectedNodes.size(); ++i)
					{
						//this is so we dont reprocess a node
						if (currentNode->connectedNodes[i]->fValue == 0)
						{
							//process the node
							currentNode->connectedNodes[i]->gValue = currentNode->gValue + 1;
							currentNode->connectedNodes[i]->fValue = currentNode->connectedNodes[i]->gValue + currentNode->connectedNodes[i]->hValue;
							openNodes.push_back(currentNode->connectedNodes[i]);
						}
					}


					if (openNodes.empty())
					{
						pathExists = false;
						continue;
					}
					cNode* nextNode = openNodes.back();
					int lowestF = nextNode->fValue;
					//now go through the open nodes and find the one with least f value 
					for (std::list<cNode*>::iterator iterator = openNodes.begin(), end = openNodes.end(); iterator != end; ++iterator) {
						if ((*iterator)->fValue < lowestF)
						{
							lowestF = (*iterator)->fValue;
							nextNode = (*iterator);
						}
						if ((*iterator) == endingNode)
						{
							nextNode = endingNode;
							break;
						}
					}
					//set that to current node
					currentNode = nextNode;
					//set the main object destination to that node
					objectDestination.x = nextNode->xLoc;
					objectDestination.y = 2;
					objectDestination.z = nextNode->zLoc;
					//set mainObjectMoving to true
					mainObjectMoving = true;
				}
				else
				{
					findingPath = false;
				}

			}
			else
			{
				//first run through current node should be the end node
				if (currentNode != startingNode)
				{
					std::vector<cNode*> backtrackNodes;
					//to pick the next node we have to pick a node that is adjacent and in the list of closed nodes
					for (int i = 0; i < currentNode->connectedNodes.size(); ++i)
					{
						if (currentNode->connectedNodes[i] == startingNode)
						{
							backtrackNodes.clear();
							backtrackNodes.push_back(currentNode->connectedNodes[i]);
							break;
						}
						if (std::find(closedNodes.begin(), closedNodes.end(), currentNode->connectedNodes[i]) != closedNodes.end())
						{
							backtrackNodes.push_back(currentNode->connectedNodes[i]);
						}
					}
					//if there are multiple then have to select the one with the lowest g value
					if (backtrackNodes.size() > 1)
					{
						cNode* selected = backtrackNodes[0];
						for (int i = 1; i < backtrackNodes.size(); ++i)
						{
							if (backtrackNodes[i]->gValue < selected->gValue)
								selected = backtrackNodes[i];
						}
						currentNode = selected;
					}
					else
					{
						currentNode = backtrackNodes[0];
					}

					objectDestination.x = currentNode->xLoc;
					objectDestination.y = 2;
					objectDestination.z = currentNode->zLoc;
					//set mainObjectMoving to true
					mainObjectMoving = true;

				}
			}
		}



		lastTimeStep = curTime;

	}// while ( ! glfwWindowShouldClose(window) )


	glfwDestroyWindow(window);
	glfwTerminate();

	// 
	delete ::g_pShaderManager;
	delete ::g_pVAOManager;

	//    exit(EXIT_SUCCESS);
	return 0;
}

void setCubeSamplerAndBlenderByIndex(GLint samplerIndex, float blendRatio, GLint textureUnitID)
{
	switch (samplerIndex)
	{
	case 0:
		glUniform1i(texSampCube00_LocID, textureUnitID);
		glUniform1f(texCubeBlend00_LocID, blendRatio);
		break;
	case 1:
		glUniform1i(texSampCube01_LocID, textureUnitID);
		glUniform1f(texCubeBlend01_LocID, blendRatio);
		break;
	case 2:
		glUniform1i(texSampCube02_LocID, textureUnitID);
		glUniform1f(texCubeBlend02_LocID, blendRatio);
		break;
	case 3:
		glUniform1i(texSampCube03_LocID, textureUnitID);
		glUniform1f(texCubeBlend03_LocID, blendRatio);
		break;
	default:
		// Invalid samplerIndex;
		break;
	}//switch (samplerIndex)
	return;
}//void setCubeSamplerAndBlenderByIndex()

