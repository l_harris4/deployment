#include "globalOpenGL_GLFW.h"
#include "globalGameStuff.h"

#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp> // glm::value_ptr
#include "sMeshDrawInfo.h"
#include <iostream>
#include <ft2build.h>
#include FT_FREETYPE_H

extern bool g_lightsOn;
extern bool g_texturesOn;
extern bool g_boundingBoxes;
extern FT_Library mft;
extern FT_Face mface;
extern GLint attribute_coord;
extern GLint uniform_igcolour;
extern GLint uniform_tex;
extern GLint uniform_color;
extern GLuint mdp_vbo;
extern GLuint textProgram;
extern GLuint mvao;
extern int selectedLanguage;
extern std::vector<std::string> loadedText;
extern bool validLicense;


struct point {
	GLfloat x;
	GLfloat y;
	GLfloat s;
	GLfloat t;
};




// Draw a single object
// If pParentGO == NULL, then IT'S the parent
void DrawObject(cGameObject* pTheGO, cGameObject* pParentGO);


// Draws one mesh in the game object. 
// The game object is passed to get the orientation.
void DrawMesh(sMeshDrawInfo &theMesh, cGameObject* pTheGO);


void RenderText(const char *text, float x, float y, float sx, float sy) {
	glDisable(GL_CULL_FACE);
	glUniform1i(uniform_igcolour, false);

	const char *p;
	FT_GlyphSlot g = mface->glyph;

	GLuint tex;
	//
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glUniform1i(uniform_tex, 0);

	/* We require 1 byte alignment when uploading texture data */
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	/* Clamping to edges is important to prevent artifacts when scaling */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	/* Linear filtering usually looks best for text */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	/* Set up the VBO for our vertex data */
	glEnableVertexAttribArray(attribute_coord);
	glBindBuffer(GL_ARRAY_BUFFER, mdp_vbo);
	glVertexAttribPointer(attribute_coord, 4, GL_FLOAT, GL_FALSE, 0, 0);


	/* Loop through all characters */
	for (p = text; *p; p++) {
		/* Try to load and render the character */
		if (FT_Load_Char(mface, *p, FT_LOAD_RENDER))
			continue;

		/* Upload the "bitmap", which contains an 8-bit grayscale image, as an alpha texture */
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, g->bitmap.width, g->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE, g->bitmap.buffer);


		/* Calculate the vertex and texture coordinates */
		float x2 = x + g->bitmap_left * sx;
		float y2 = -y - g->bitmap_top * sy;
		float w = g->bitmap.width * sx;
		float h = g->bitmap.rows * sy;

		point box[4] = {
			{ x2, -y2, 0, 0 },
			{ x2 + w, -y2, 1, 0 },
			{ x2, -y2 - h, 0, 1 },
			{ x2 + w, -y2 - h, 1, 1 },
		};

		/* Draw the character on the screen */
		glBufferData(GL_ARRAY_BUFFER, sizeof(box), box, GL_DYNAMIC_DRAW);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		/* Advance the cursor to the start of the next character */
		x += (g->advance.x >> 6) * sx;
		y += (g->advance.y >> 6) * sy;
	}

	glDisableVertexAttribArray(attribute_coord);
	glDeleteTextures(1, &tex);


}



void RenderScene(std::vector< cGameObject* > &vec_pGOs, GLFWwindow* pGLFWWindow, double deltaTime)
{

	float ratio;
	int width, height;
	glm::mat4x4 matProjection;			// was "p"

	glfwGetFramebufferSize(pGLFWWindow, &width, &height);
	ratio = width / (float)height;
	glViewport(0, 0, width, height);

	// Clear colour AND depth buffer
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glEnable(GL_DEPTH_TEST);

	//        glUseProgram(program);
	::g_pShaderManager->useShaderProgram("mySexyShader");
	GLint curShaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	// Update all the light uniforms...
	// (for the whole scene)
	::g_pLightManager->CopyLightInformationToCurrentShader();

	// Projection and view don't change per scene (maybe)
	matProjection = glm::perspective(0.6f,			// FOV
		ratio,		// Aspect ratio
		1.0f,			// Near (as big as possible)
		100000.0f);	// Far (as small as possible)


					// View or "camera" matrix
	glm::mat4 matView = glm::mat4(1.0f);	// was "v"

											// Now the veiw matrix is taken right from the camera class
	matView = ::g_pTheCamera->getViewMatrix();
	//matView = glm::lookAt( g_cameraXYZ,						// "eye" or "camera" position
	//					   g_cameraTarget_XYZ,		// "At" or "target" 
	//					   glm::vec3( 0.0f, 1.0f, 0.0f ) );	// "up" vector

	GLint uniLoc_mView = glGetUniformLocation(curShaderID, "mView");
	GLint uniLoc_mProjection = glGetUniformLocation(curShaderID, "mProjection");

	glUniformMatrix4fv(uniLoc_mView, 1, GL_FALSE,
		(const GLfloat*)glm::value_ptr(matView));
	glUniformMatrix4fv(uniLoc_mProjection, 1, GL_FALSE,
		(const GLfloat*)glm::value_ptr(matProjection));


	// Enable blend ("alpha") transparency for the scene
	// NOTE: You "should" turn this OFF, then draw all NON-Transparent objects
	//       Then turn ON, sort objects from far to near ACCORDING TO THE CAMERA
	//       and draw them
	glEnable(GL_BLEND);		// Enables "blending"
							//glDisable( GL_BLEND );
							// Source == already on framebuffer
							// Dest == what you're about to draw
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);



	// Draw the scene
	if (validLicense)
	{
		unsigned int sizeOfVector = (unsigned int)::g_vecGameObjects.size();	//*****//
		for (int index = 0; index != sizeOfVector; index++)
		{
			cGameObject* pTheGO = ::g_vecGameObjects[index];

			// This is the top level vector, so they are all "parents" 
			DrawObject(pTheGO, NULL);

		}//for ( int index = 0...
	}

	if (!validLicense) //6 is a sentinel value saying that no text should be rendered
	{
		glUseProgram(textProgram);
		GLfloat usedColour[4] = { 1, 0, 0, 1 };

		glUniform4fv(uniform_color, 1, usedColour);

		glBindVertexArray(mvao);

		RenderText("NOT VALID LICENSE", -0.7, 0.5 - (0.1), 0.001, 0.001);
	}
	return;
}

// Draw a single object
// If pParentGO == NULL, then IT'S the parent
void DrawObject(cGameObject* pTheGO, cGameObject* pParentGO)
{
	if (!pTheGO)
	{	// Shouldn't happen, but if GO pointer is invlaid, return
		return;
	}
	// Is there a game object? 
	if (!pTheGO->bIsVisible)	//if ( ::g_GameObjects[index] == 0 )
	{	// Nothing to draw

		// Draw any children
		unsigned int numChildGOs = (unsigned int)pTheGO->vec_pChildObjects.size();
		for (unsigned int index = 0; index != numChildGOs; index++)
		{
			DrawObject(pTheGO->vec_pChildObjects[index], pTheGO);
		}
	}

	// Go through all meshes, drawing them
	unsigned int numMeshes = (unsigned int)pTheGO->vecMeshes.size();
	for (unsigned int meshIndex = 0; meshIndex != numMeshes; meshIndex++)
	{
		DrawMesh(pTheGO->vecMeshes[meshIndex], pTheGO);
	}

	return;
}

// Very quick and dirty way of connecting the samplers
namespace QnDTexureSamplerUtility
{
	void LoadUniformLocationsIfNeeded(unsigned int shaderID);
	void setAllSamplerUnitsToInvalidNumber(GLint invalidTextureUnit);
	void clearAllBlendValuesToZero(void);
	void SetSamplersForMeshTextures(sMeshDrawInfo &meshDrawInfo,
		std::map<std::string /*textureName*/, CTexUnitInfoBrief> &mapTexAndUnitInfo);
	//	void set2DSamplerAndBlenderByIndex(GLint samplerIndex, float blendRatio, GLint textureUnitID );
	//	void setCubeSamplerAndBlenderByIndex( GLint samplerIndex, float blendRatio, GLint textureUnitID );
};

// Draws one mesh in the game object. 
// The game object is passed to get the orientation.
void DrawMesh(sMeshDrawInfo &theMesh, cGameObject* pTheGO)
{
	if (!theMesh.bIsVisible)
	{
		return;
	}

	if (pTheGO->friendlyName == LEFTTEAPOTNAME)
	{
		int sexyBreakpoint = 0;
	}


	sVAOInfo VAODrawInfo;
	if (::g_pVAOManager->lookupVAOFromName(theMesh.name, VAODrawInfo) == false)
	{	// Didn't find mesh
		//std::cout << "WARNING: Didn't find mesh " << meshToDraw << " in VAO Manager" << std::endl;
		return;
	}

	// 'model' or 'world' matrix
	glm::mat4x4 mModel = glm::mat4x4(1.0f);	//		mat4x4_identity(m);

	glm::mat4 trans = glm::mat4x4(1.0f);
	trans = glm::translate(trans,
		theMesh.offset + pTheGO->position);
	mModel = mModel * trans;

	glm::mat4 postRotQuat = glm::mat4(pTheGO->getFinalMeshQOrientation(theMesh.getQOrientation()));
	mModel = mModel * postRotQuat;

	glm::mat4 matScale = glm::mat4x4(1.0f);
	matScale = glm::scale(matScale,
		glm::vec3(pTheGO->scale,
			pTheGO->scale,
			pTheGO->scale));
	mModel = mModel * matScale;

	GLint curShaderProgID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	GLint uniLoc_mModel = glGetUniformLocation(curShaderProgID, "mModel");
	glUniformMatrix4fv(uniLoc_mModel, 1, GL_FALSE,
		(const GLfloat*)glm::value_ptr(mModel));

	glm::mat4 mWorldInTranpose = glm::inverse(glm::transpose(mModel));
	GLint uniLoc_mWorldInvTrans = glGetUniformLocation(curShaderProgID, "mWorldInvTranspose");
	glUniformMatrix4fv(uniLoc_mWorldInvTrans, 1, GL_FALSE,
		(const GLfloat*)glm::value_ptr(mWorldInTranpose));

	// Other uniforms:
	GLint uniLoc_eyePosition = glGetUniformLocation(curShaderProgID, "eyePosition");
	glUniform3f(uniLoc_eyePosition,
		::g_pTheCamera->eye.x, ::g_pTheCamera->eye.y, ::g_pTheCamera->eye.z);


	// Diffuse is often 0.2-0.3 the value of the diffuse
	GLint uniLoc_ambientToDiffuseRatio = glGetUniformLocation(curShaderProgID, "ambientToDiffuseRatio");
	glUniform1f(uniLoc_ambientToDiffuseRatio, 0.2f);

	// Specular: For now, set this colour to white, and the shininess to something high 
	//	it's an exponent so 64 is pretty shinny (1.0 is "flat", 128 is excessively shiny)
	GLint uniLoc_materialSpecular = glGetUniformLocation(curShaderProgID, "materialSpecular");
	glUniform4f(uniLoc_materialSpecular, 1.0f, 1.0f, 1.0f, 64.0f);

	GLint uniLoc_materialDiffuse = glGetUniformLocation(curShaderProgID, "materialDiffuse");

	glUniform4f(uniLoc_materialDiffuse,
		pTheGO->diffuseColour.r,
		pTheGO->diffuseColour.g,
		pTheGO->diffuseColour.b,
		pTheGO->diffuseColour.a);

	GLint textBlend00_ID = glGetUniformLocation(curShaderProgID, "texBlend00");
	GLint textBlend01_ID = glGetUniformLocation(curShaderProgID, "texBlend01");

	glUniform1f(textBlend00_ID, pTheGO->textureBlend[0]);
	glUniform1f(textBlend01_ID, pTheGO->textureBlend[1]);


	// Set ALL the samplers to something (so they don't point to GL_TEXTURE0)
	QnDTexureSamplerUtility::LoadUniformLocationsIfNeeded(curShaderProgID);
	// There's no way we have 999 texture units...
	QnDTexureSamplerUtility::setAllSamplerUnitsToInvalidNumber(999);
	QnDTexureSamplerUtility::clearAllBlendValuesToZero();

	// Now set our samplers, and blend function to something
	// Basic texture binding setup (assign all loaded textures to samplers)
	g_pTextureManager->UpdateTextureBindingsByTextureNameSimple();
	std::map<std::string, CTexUnitInfoBrief> mapTexNameToTexUnit;
	g_pTextureManager->GetBoundTextureUnitsByTextureNames(mapTexNameToTexUnit);

	// Now look up what textures our object is using and set the samplers
	QnDTexureSamplerUtility::SetSamplersForMeshTextures(theMesh, mapTexNameToTexUnit);

	// ***************************************
	// Texture based height map example code
	//	uniform sampler2D texHeightMap;
	//	uniform bool bIsHeightMap;
	GLint texHeightMap_UniLoc = glGetUniformLocation(curShaderProgID, "texHeightMap");
	GLint bIsHeightMap_UniLoc = glGetUniformLocation(curShaderProgID, "bIsHeightMap");

	{
		glUniform1f(bIsHeightMap_UniLoc, GL_FALSE);
	}

	GLint uniLoc_bIsSkyBoxObject = glGetUniformLocation(curShaderProgID, "isASkyBox");
	if (theMesh.bIsSkyBoxObject)
	{
		glUniform1f(uniLoc_bIsSkyBoxObject, GL_TRUE);
	}
	else
	{
		glUniform1f(uniLoc_bIsSkyBoxObject, GL_FALSE);
	}

	GLint uniLoc_bUsingLighting = glGetUniformLocation(curShaderProgID, "bUsingLighting");
	if (g_lightsOn)
	{
		glUniform1f(uniLoc_bUsingLighting, 1.0f);	// TRUE
	}
	else
	{
		glUniform1f(uniLoc_bUsingLighting, 0.0f);	// FALSE
	}

	GLint uniLoc_bUsingTextures = glGetUniformLocation(curShaderProgID, "bUsingTextures");
	if (g_texturesOn)
	{
		glUniform1f(uniLoc_bUsingTextures, 1.0f);	// TRUE
	}
	else
	{
		glUniform1f(uniLoc_bUsingTextures, 0.0f);	// FALSE
	}

	GLint uniLoc_bDiscardTexture = glGetUniformLocation(curShaderProgID, "bDiscardTexture");
	if (pTheGO->bDiscardTexture)
	{
		glUniform1f(uniLoc_bDiscardTexture, 1.0f);	// TRUE
	}
	else
	{
		glUniform1f(uniLoc_bDiscardTexture, 0.0f);	// FALSE
	}

	GLint uniLoc_bTransparentTexture = glGetUniformLocation(curShaderProgID, "bTransparentTexture");
	if (pTheGO->bTransparentTexture)
	{
		glUniform1f(uniLoc_bTransparentTexture, 1.0f);	// TRUE
	}
	else
	{
		glUniform1f(uniLoc_bTransparentTexture, 0.0f);	// FALSE
	}


	GLint uniLoc_bIsDebugWireFrameObject = glGetUniformLocation(curShaderProgID, "bIsDebugWireFrameObject");

	// Wireframe?
	if (theMesh.bDrawAsWireFrame)
	{
		glUniform1f(uniLoc_bIsDebugWireFrameObject, 1.0f);	// TRUE
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);	// Default
	}
	else
	{
		glUniform1f(uniLoc_bIsDebugWireFrameObject, 0.0f);	// FALSE
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);	// Default
	}

	// GL_CULL_FACE
	if (theMesh.bDisableBackFaceCulling)
	{
		glDisable(GL_CULL_FACE);
	}
	else
	{
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}// GL_CULL_FACE

	 // GL_DEPTH_TEST
	if (theMesh.bDisableDepthTest)
	{
		glDisable(GL_DEPTH_TEST);		// Test for z and store in z buffer
	}
	else
	{
		glEnable(GL_DEPTH_TEST);		// Test for z and store in z buffer
	}// GL_DEPTH_TEST


	 // STARTOF: Reflection and refraction shader uniforms
	GLint isReflectRefract_UniLoc = glGetUniformLocation(curShaderProgID, "isReflectRefract");
	GLint reflectBlendRatio_UniLoc = glGetUniformLocation(curShaderProgID, "reflectBlendRatio");		// How much reflection (0-1)
	GLint refractBlendRatio_UniLoc = glGetUniformLocation(curShaderProgID, "refractBlendRatio");		// How much refraction (0-1)
	GLint coefficientRefract_UniLoc = glGetUniformLocation(curShaderProgID, "coefficientRefract");		// coefficient of refraction 

																										// Is this the reflective sphere
	if (theMesh.bIsEnvirMapped)
	{
		glUniform1f(isReflectRefract_UniLoc, GL_TRUE);
	}
	else
	{
		glUniform1f(isReflectRefract_UniLoc, GL_FALSE);
	}
	// 
	glUniform1f(reflectBlendRatio_UniLoc, theMesh.reflectBlendRatio);
	glUniform1f(refractBlendRatio_UniLoc, theMesh.refractBlendRatio);
	glUniform1f(coefficientRefract_UniLoc, theMesh.coefficientRefract);
	// And more environment things

	glBindVertexArray(VAODrawInfo.VAO_ID);

	glDrawElements(GL_TRIANGLES,
		VAODrawInfo.numberOfIndices,		// testMesh.numberOfTriangles * 3,	// How many vertex indices
		GL_UNSIGNED_INT,					// 32 bit int 
		0);
	// Unbind that VAO
	glBindVertexArray(0);

	return;
}



// Very quick and dirty way of connecting the samplers
namespace QnDTexureSamplerUtility
{
	bool bIsLoaded = false;

	// 2D textures:
	GLint texSamp2D00_LocID = -1;
	GLint texSamp2D01_LocID = -1;
	GLint texSamp2D02_LocID = -1;
	GLint texSamp2D03_LocID = -1;
	GLint texSamp2D04_LocID = -1;
	GLint texSamp2D05_LocID = -1;
	GLint texSamp2D06_LocID = -1;
	GLint texSamp2D07_LocID = -1;

	GLint texBlend00_LocID = -1;
	GLint texBlend01_LocID = -1;
	GLint texBlend02_LocID = -1;
	GLint texBlend03_LocID = -1;
	GLint texBlend04_LocID = -1;
	GLint texBlend05_LocID = -1;
	GLint texBlend06_LocID = -1;
	GLint texBlend07_LocID = -1;

	// Cube map textures
	GLint texSampCube00_LocID = -1;
	GLint texSampCube01_LocID = -1;
	GLint texSampCube02_LocID = -1;
	GLint texSampCube03_LocID = -1;

	GLint texCubeBlend00_LocID = -1;
	GLint texCubeBlend01_LocID = -1;
	GLint texCubeBlend02_LocID = -1;
	GLint texCubeBlend03_LocID = -1;

	void LoadUniformLocationsIfNeeded(unsigned int shaderID)
	{
		if (!bIsLoaded)
		{	// Load the uniform locations
			texSamp2D00_LocID = glGetUniformLocation(shaderID, "texSamp2D00");
			texSamp2D01_LocID = glGetUniformLocation(shaderID, "texSamp2D01");
			texSamp2D02_LocID = glGetUniformLocation(shaderID, "texSamp2D02");
			texSamp2D03_LocID = glGetUniformLocation(shaderID, "texSamp2D03");
			texSamp2D04_LocID = glGetUniformLocation(shaderID, "texSamp2D04");
			texSamp2D05_LocID = glGetUniformLocation(shaderID, "texSamp2D05");
			texSamp2D06_LocID = glGetUniformLocation(shaderID, "texSamp2D06");
			texSamp2D07_LocID = glGetUniformLocation(shaderID, "texSamp2D07");

			texBlend00_LocID = glGetUniformLocation(shaderID, "texBlend00");
			texBlend01_LocID = glGetUniformLocation(shaderID, "texBlend01");
			texBlend02_LocID = glGetUniformLocation(shaderID, "texBlend02");
			texBlend03_LocID = glGetUniformLocation(shaderID, "texBlend03");
			texBlend04_LocID = glGetUniformLocation(shaderID, "texBlend04");
			texBlend05_LocID = glGetUniformLocation(shaderID, "texBlend05");
			texBlend06_LocID = glGetUniformLocation(shaderID, "texBlend06");
			texBlend07_LocID = glGetUniformLocation(shaderID, "texBlend07");

			texSampCube00_LocID = glGetUniformLocation(shaderID, "texSampCube00");
			texSampCube01_LocID = glGetUniformLocation(shaderID, "texSampCube00");
			texSampCube02_LocID = glGetUniformLocation(shaderID, "texSampCube00");
			texSampCube03_LocID = glGetUniformLocation(shaderID, "texSampCube00");

			texCubeBlend00_LocID = glGetUniformLocation(shaderID, "texCubeBlend00");
			texCubeBlend01_LocID = glGetUniformLocation(shaderID, "texCubeBlend01");
			texCubeBlend02_LocID = glGetUniformLocation(shaderID, "texCubeBlend02");
			texCubeBlend03_LocID = glGetUniformLocation(shaderID, "texCubeBlend03");

			bIsLoaded = true;	// So we won't call these again
		}

		return;
	}// void LoadUniformLocationsIfNeeded()


	void setAllSamplerUnitsToInvalidNumber(GLint invalidTextureUnit)
	{
		glUniform1i(texSamp2D00_LocID, invalidTextureUnit);
		glUniform1i(texSamp2D01_LocID, invalidTextureUnit);
		glUniform1i(texSamp2D02_LocID, invalidTextureUnit);
		glUniform1i(texSamp2D03_LocID, invalidTextureUnit);
		glUniform1i(texSamp2D04_LocID, invalidTextureUnit);
		glUniform1i(texSamp2D05_LocID, invalidTextureUnit);
		glUniform1i(texSamp2D06_LocID, invalidTextureUnit);
		glUniform1i(texSamp2D07_LocID, invalidTextureUnit);

		// Cube map textures
		glUniform1i(texSampCube00_LocID, invalidTextureUnit);
		glUniform1i(texSampCube01_LocID, invalidTextureUnit);
		glUniform1i(texSampCube02_LocID, invalidTextureUnit);
		glUniform1i(texSampCube03_LocID, invalidTextureUnit);

		return;
	}//void setAllSamplerUnitsToInvalidNumber()

	void clearAllBlendValuesToZero(void)
	{
		glUniform1f(texBlend00_LocID, 0.0f);
		glUniform1f(texBlend01_LocID, 0.0f);
		glUniform1f(texBlend02_LocID, 0.0f);
		glUniform1f(texBlend03_LocID, 0.0f);
		glUniform1f(texBlend04_LocID, 0.0f);
		glUniform1f(texBlend05_LocID, 0.0f);
		glUniform1f(texBlend06_LocID, 0.0f);
		glUniform1f(texBlend07_LocID, 0.0f);

		glUniform1f(texCubeBlend00_LocID, 0.0f);
		glUniform1f(texCubeBlend01_LocID, 0.0f);
		glUniform1f(texCubeBlend02_LocID, 0.0f);
		glUniform1f(texCubeBlend03_LocID, 0.0f);

		return;
	}//void clearAllBlendValuesToZero()

	 // Handy function to set sampelrs
	void set2DSamplerAndBlenderByIndex(GLint samplerIndex, float blendRatio, GLint textureUnitID)
	{
		switch (samplerIndex)
		{
		case 0:
			glUniform1i(texSamp2D00_LocID, textureUnitID);
			glUniform1f(texBlend00_LocID, blendRatio);
			break;
		case 1:
			glUniform1i(texSamp2D01_LocID, textureUnitID);
			glUniform1f(texBlend01_LocID, blendRatio);
			break;
		case 2:
			glUniform1i(texSamp2D02_LocID, textureUnitID);
			glUniform1f(texBlend02_LocID, blendRatio);
			break;
		case 3:
			glUniform1i(texSamp2D03_LocID, textureUnitID);
			glUniform1f(texBlend03_LocID, blendRatio);
			break;
		case 4:
			glUniform1i(texSamp2D04_LocID, textureUnitID);
			glUniform1f(texBlend04_LocID, blendRatio);
			break;
		case 5:
			glUniform1i(texSamp2D05_LocID, textureUnitID);
			glUniform1f(texBlend05_LocID, blendRatio);
			break;
		case 6:
			glUniform1i(texSamp2D06_LocID, textureUnitID);
			glUniform1f(texBlend06_LocID, blendRatio);
			break;
		case 7:
			glUniform1i(texSamp2D07_LocID, textureUnitID);
			glUniform1f(texBlend07_LocID, blendRatio);
			break;
		default:
			// Invalid samplerIndex
			break;
		}//switch (samplerIndex)
		return;
	}//void set2DSamplerAndBlenderByIndex()

	void setCubeSamplerAndBlenderByIndex(GLint samplerIndex, float blendRatio, GLint textureUnitID)
	{
		switch (samplerIndex)
		{
		case 0:
			glUniform1i(texSampCube00_LocID, textureUnitID);
			glUniform1f(texCubeBlend00_LocID, blendRatio);
			break;
		case 1:
			glUniform1i(texSampCube01_LocID, textureUnitID);
			glUniform1f(texCubeBlend01_LocID, blendRatio);
			break;
		case 2:
			glUniform1i(texSampCube02_LocID, textureUnitID);
			glUniform1f(texCubeBlend02_LocID, blendRatio);
			break;
		case 3:
			glUniform1i(texSampCube03_LocID, textureUnitID);
			glUniform1f(texCubeBlend03_LocID, blendRatio);
			break;
		default:
			// Invalid samplerIndex;
			break;
		}//switch (samplerIndex)
		return;
	}//void setCubeSamplerAndBlenderByIndex()


	void SetSamplersForMeshTextures(sMeshDrawInfo &meshDrawInfo,
		std::map<std::string /*textureName*/, CTexUnitInfoBrief> &mapTexAndUnitInfo)
	{
		// 2D textures first
		int numTextures = (int)meshDrawInfo.vecMehs2DTextures.size();
		for (int samplerIndex = 0; samplerIndex != numTextures; samplerIndex++)
		{
			// What texture unit is this texture set to?
			std::map<std::string, CTexUnitInfoBrief>::iterator itTextUnitInfo
				= mapTexAndUnitInfo.find(meshDrawInfo.vecMehs2DTextures[samplerIndex].name);
			// Have we mapped that one?
			if (itTextUnitInfo != mapTexAndUnitInfo.end())
			{	// Yes, so assign it
				set2DSamplerAndBlenderByIndex(samplerIndex,
					meshDrawInfo.vecMehs2DTextures[samplerIndex].blendRatio,
					itTextUnitInfo->second.texUnitNumOffsetBy_GL_TEXTURE0);
				// Set blend function, too
			}
		}// 2D textures

		 // Now cube maps
		numTextures = (int)meshDrawInfo.vecMeshCubeMaps.size();
		for (int samplerIndex = 0; samplerIndex != numTextures; samplerIndex++)
		{
			// What texture unit is this texture set to?
			std::map<std::string, CTexUnitInfoBrief>::iterator itTextUnitInfo
				= mapTexAndUnitInfo.find(meshDrawInfo.vecMeshCubeMaps[samplerIndex].name);
			// Have we mapped that one?
			if (itTextUnitInfo != mapTexAndUnitInfo.end())
			{	// Yes, so assign it
				setCubeSamplerAndBlenderByIndex(samplerIndex,
					meshDrawInfo.vecMeshCubeMaps[samplerIndex].blendRatio,
					itTextUnitInfo->second.texUnitNumOffsetBy_GL_TEXTURE0);
			}
		}// cube maps



	}//void SetSamplersForMeshTextures()

};//namespace QnDTexureSamplerUtility
