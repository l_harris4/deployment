Object Start
normals: true
physics: false
wireframe: false
texture1: crateT.bmp
texture1Strength: 1.0
filename: crate.ply
position: 9.7,-400,8.5
orientation: 0,0,0
scale: 1
colour: 0.7,0.4,0.3,1.0
Object End

Object Start
normals: true
physics: false
wireframe: false
texture1: FlagT.bmp
texture1Strength: 1.0
filename: Flag.ply
position: 9.7,-400,8.5
orientation: -90,0,0
scale: 0.1
colour: 0.7,0.4,0.3,1.0
Object End

Object Start
normals: true
physics: false
wireframe: false
texture1: FlagET.bmp
texture1Strength: 1.0
filename: Flag.ply
position: 9.7,-400,8.5
orientation: -90,-90,0
scale: 0.1
colour: 0.7,0.4,0.3,1.0
Object End

Object Start
normals: true
physics: false
wireframe: false
texture1: FlagST.bmp
texture1Strength: 1.0
filename: Flag.ply
position: 9.7,-400,8.5
orientation: -90,0,0
scale: 0.1
colour: 0.7,0.4,0.3,1.0
Object End

Node Destination: 12,20
Node Origin: 0,0

//
Node Start
nPoint: -4,0
nValid: true
Node End

Node Start
nPoint: -4,2
nValid: true
Node End

Node Start
nPoint: -4,4
nValid: true
Node End

Node Start
nPoint: -4,6
nValid: true
Node End

Node Start
nPoint: -4,8
nValid: true
Node End

Node Start
nPoint: -4,10
nValid: true
Node End

Node Start
nPoint: -4,12
nValid: true
Node End

Node Start
nPoint: -4,14
nValid: false
Node End

Node Start
nPoint: -4,16
nValid: true
Node End

Node Start
nPoint: -4,18
nValid: true
Node End

Node Start
nPoint: -4,20
nValid: true
Node End

Node Start
nPoint: -4,22
nValid: true
Node End
//
Node Start
nPoint: -2,0
nValid: true
Node End

Node Start
nPoint: -2,2
nValid: false
Node End

Node Start
nPoint: -2,4
nValid: true
Node End

Node Start
nPoint: -2,6
nValid: true
Node End

Node Start
nPoint: -2,8
nValid: true
Node End

Node Start
nPoint: -2,10
nValid: true
Node End

Node Start
nPoint: -2,12
nValid: true
Node End

Node Start
nPoint: -2,14
nValid: false
Node End

Node Start
nPoint: -2,16
nValid: true
Node End

Node Start
nPoint: -2,18
nValid: true
Node End

Node Start
nPoint: -2,20
nValid: false
Node End

Node Start
nPoint: -2,22
nValid: true
Node End
//

Node Start
nPoint: 0,0
nValid: true
Node End

Node Start
nPoint: 0,2
nValid: false
Node End

Node Start
nPoint: 0,4
nValid: true
Node End

Node Start
nPoint: 0,6
nValid: true
Node End

Node Start
nPoint: 0,8
nValid: true
Node End

Node Start
nPoint: 0,10
nValid: true
Node End

Node Start
nPoint: 0,12
nValid: true
Node End

Node Start
nPoint: 0,14
nValid: false
Node End

Node Start
nPoint: 0,16
nValid: true
Node End

Node Start
nPoint: 0,18
nValid: false
Node End

Node Start
nPoint: 0,20
nValid: true
Node End

Node Start
nPoint: 0,22
nValid: true
Node End

//
Node Start
nPoint: 2,0
nValid: true
Node End

Node Start
nPoint: 2,2
nValid: false
Node End

Node Start
nPoint: 2,4
nValid: true
Node End

Node Start
nPoint: 2,6
nValid: true
Node End

Node Start
nPoint: 2,8
nValid: false
Node End

Node Start
nPoint: 2,10
nValid: true
Node End

Node Start
nPoint: 2,12
nValid: true
Node End

Node Start
nPoint: 2,14
nValid: false
Node End

Node Start
nPoint: 2,16
nValid: true
Node End

Node Start
nPoint: 2,18
nValid: true
Node End

Node Start
nPoint: 2,20
nValid: false
Node End

Node Start
nPoint: 2,22
nValid: true
Node End

//

Node Start
nPoint: 4,0
nValid: true
Node End

Node Start
nPoint: 4,2
nValid: false
Node End

Node Start
nPoint: 4,4
nValid: true
Node End

Node Start
nPoint: 4,6
nValid: false
Node End

Node Start
nPoint: 4,8
nValid: false
Node End

Node Start
nPoint: 4,10
nValid: true
Node End

Node Start
nPoint: 4,12
nValid: true
Node End

Node Start
nPoint: 4,14
nValid: false
Node End

Node Start
nPoint: 4,16
nValid: true
Node End

Node Start
nPoint: 4,18
nValid: true
Node End

Node Start
nPoint: 4,20
nValid: false
Node End

Node Start
nPoint: 4,22
nValid: true
Node End

//

Node Start
nPoint: 6,0
nValid: true
Node End

Node Start
nPoint: 6,2
nValid: true
Node End

Node Start
nPoint: 6,4
nValid: true
Node End

Node Start
nPoint: 6,6
nValid: false
Node End

Node Start
nPoint: 6,8
nValid: true
Node End

Node Start
nPoint: 6,10
nValid: true
Node End

Node Start
nPoint: 6,12
nValid: true
Node End

Node Start
nPoint: 6,14
nValid: false
Node End

Node Start
nPoint: 6,16
nValid: true
Node End

Node Start
nPoint: 6,18
nValid: true
Node End

Node Start
nPoint: 6,20
nValid: false
Node End

Node Start
nPoint: 6,22
nValid: true
Node End

//

Node Start
nPoint: 8,0
nValid: true
Node End

Node Start
nPoint: 8,2
nValid: false
Node End

Node Start
nPoint: 8,4
nValid: true
Node End

Node Start
nPoint: 8,6
nValid: false
Node End

Node Start
nPoint: 8,8
nValid: true
Node End

Node Start
nPoint: 8,10
nValid: true
Node End

Node Start
nPoint: 8,12
nValid: true
Node End

Node Start
nPoint: 8,14
nValid: false
Node End

Node Start
nPoint: 8,16
nValid: true
Node End

Node Start
nPoint: 8,18
nValid: true
Node End

Node Start
nPoint: 8,20
nValid: false
Node End

Node Start
nPoint: 8,22
nValid: true
Node End

//

Node Start
nPoint: 10,0
nValid: true
Node End

Node Start
nPoint: 10,2
nValid: false
Node End

Node Start
nPoint: 10,4
nValid: true
Node End

Node Start
nPoint: 10,6
nValid: true
Node End

Node Start
nPoint: 10,8
nValid: true
Node End

Node Start
nPoint: 10,10
nValid: true
Node End

Node Start
nPoint: 10,12
nValid: true
Node End

Node Start
nPoint: 10,14
nValid: true
Node End

Node Start
nPoint: 10,16
nValid: true
Node End

Node Start
nPoint: 10,18
nValid: true
Node End

Node Start
nPoint: 10,20
nValid: false
Node End

Node Start
nPoint: 10,22
nValid: true
Node End


//

Node Start
nPoint: 12,0
nValid: true
Node End

Node Start
nPoint: 12,2
nValid: false
Node End

Node Start
nPoint: 12,4
nValid: true
Node End

Node Start
nPoint: 12,6
nValid: true
Node End

Node Start
nPoint: 12,8
nValid: true
Node End

Node Start
nPoint: 12,10
nValid: true
Node End

Node Start
nPoint: 12,12
nValid: true
Node End

Node Start
nPoint: 12,14
nValid: true
Node End

Node Start
nPoint: 12,16
nValid: true
Node End

Node Start
nPoint: 12,18
nValid: false
Node End

Node Start
nPoint: 12,20
nValid: true
Node End

Node Start
nPoint: 12,22
nValid: true
Node End





Object Start
normals: true
physics: false
wireframe: false
skybox: true
filename: SmoothSphere_Inverted_Normals_xyz_n.ply
position: 0,0,0
orientation: 0,0,0
scale: 300
colour: 1,1,1,1.0
Object End


Light Start
colour_l: 1,1,1
position_l: 0.4,20.1,0
attentuation: 0,0.09,0
Light End

Light Start
colour_l: 1,1,1
position_l: 0.4,20.1,20
attentuation: 0,0.09,0
Light End
