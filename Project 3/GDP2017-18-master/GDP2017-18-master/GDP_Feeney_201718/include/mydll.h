#ifdef MYD_LLLIBRARY_EXPORTS
#define MYDLL_LIBRARY_API __declspec(dllexport)
#else
#define MYDLL_LIBRARY_API __declspec(dllimport)
#endif // !MYDLLLIBRARY_EXPORTS

namespace MyMath {
	class Functions
	{
	public:

		static MYDLL_LIBRARY_API int add(int first, int second);//1
		static MYDLL_LIBRARY_API int subtract(int first, int second);//2
		static MYDLL_LIBRARY_API int multiply(int first, int second);//3
		static MYDLL_LIBRARY_API int divide(int first, int second);//4
		static MYDLL_LIBRARY_API float divide(float first, float second);//5
		static MYDLL_LIBRARY_API bool greaterThan(int first, int second);//6
		static MYDLL_LIBRARY_API bool lessThan(int first, int second);//7
		static MYDLL_LIBRARY_API bool equal(int first, int second);//7
	};
}